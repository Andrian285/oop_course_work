import * as mongoose from 'mongoose'

const userSchema = new mongoose.Schema({
    username: String,
    name: String,
    passwordHash: String,
    email: String,
    role: String,
    centralizedBudget: String,
    currency: String
})

const UserModel = mongoose.model('User', userSchema)

export default UserModel
