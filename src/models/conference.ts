import * as mongoose from 'mongoose'

const conferenceSchema = new mongoose.Schema({
    name: String,
    date: Date,
    users: [{
        pib: String,
        id: mongoose.Schema.Types.ObjectId
    }],
    sections: [String],
    organizer: mongoose.Schema.Types.ObjectId,
    type: String,
    informationalList: String
})

const ConferenceModel = mongoose.model('Conference', conferenceSchema)

export default ConferenceModel
