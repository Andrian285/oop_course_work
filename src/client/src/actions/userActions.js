export const setAuthenticated = authenticated => {
    return { type: 'SET_AUTHENTICATED', authenticated }
}

export const setOrganizer = isOrganizer => {
    return { type: 'SET_ORGANIZER', isOrganizer }
}

export const setUserId = userId => {
    return { type: 'SET_USER_ID', userId }
}

export const setUpdateUser = update => {
    return { type: 'UPDATE_USER', update }
}

export const setBudget = budget => {
    return { type: 'SET_BUDGET', budget }
}
