export const setLoginModal = open => {
    return { type: 'OPEN_LOGIN', open }
}

export const setSignupModal = open => {
    return { type: 'OPEN_SIGNUP', open }
}

export const setOptionsModal = open => {
    return { type: 'OPEN_OPTIONS', open }
}