export const setConferenceModal = openConferenceModal => {
    return { type: 'SET_CONFERENCE_MODAL', openConferenceModal }
}

export const setRefreshConferences = refreshConferences => {
    return { type: 'SET_REFRESH_CONFERENCES', refreshConferences }
}

export const setConferences = conferences => {
    return { type: 'SET_CONFERENCES', conferences }
}

export const updateParticipationFee = update => {
    return { type: 'UPDATE_PARTICIPATION_FEE', update }
}