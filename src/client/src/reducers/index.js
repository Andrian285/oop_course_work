export default function taskReducer(state = [], action) {
    switch (action.type) {
        case 'OPEN_SIGNUP':
            return { ...state, openSignup: action.open }
        case 'OPEN_LOGIN':
            return { ...state, openLogin: action.open }
        case 'OPEN_OPTIONS':
            return { ...state, openOptions: action.open }
        case 'SET_AUTHENTICATED':
            return { ...state, authenticated: action.authenticated }
        case 'SET_ORGANIZER':
            return { ...state, isOrganizer: action.isOrganizer }
        case 'SET_CONFERENCE_MODAL':
            return { ...state, openConferenceModal: action.openConferenceModal }
        case 'SET_REFRESH_CONFERENCES':
            return { ...state, refreshConferences: action.refreshConferences }
        case 'SET_USER_ID':
            return { ...state, userId: action.userId }
        case 'SET_CONFERENCES':
            return { ...state, conferences: action.conferences }
        case 'SET_BUDGET':
            return { ...state, budget: action.budget }
        case 'UPDATE_USER':
            return { ...state, updateUser: action.update }
        case 'UPDATE_PARTICIPATION_FEE':
            return { ...state, updateFee: action.update }
        default:
            return state;
    }
}