import React from 'react'
import NavBar from '../containers/navbar'
import { mainInfo } from '../utils/mainInfo'
import '../stylesheets/home.css'

const Home = () => (
    <div>
        <NavBar />
        <h1 className='mainInfo title'>Organization of conferences</h1>
        <div className='mainInfo info'>{mainInfo}</div>
    </div>
)

export default Home