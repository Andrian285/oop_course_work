import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import Reducer from '../reducers'
import Home from './home'
import Conferences from '../containers/conferences'
import ErrPage from './error'
import Conference from '../containers/conference'

const store = createStore(Reducer)

const App = () => {
    return (
        <Provider store={store}>
            <MuiThemeProvider>
                <BrowserRouter>
                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route exact path="/conferences" component={Conferences} />
                        <Route exact path="/conferences/:id" component={Conference} />
                        <Route component={ErrPage} />
                    </Switch>
                </BrowserRouter>
            </MuiThemeProvider>
        </Provider>
    )
}

export default App