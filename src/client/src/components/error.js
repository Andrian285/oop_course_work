import React from 'react'
import NavBar from '../containers/navbar'
import { withRouter } from 'react-router-dom'
import { errStatus } from '../utils/errorStatuses'
import '../stylesheets/error.css'

function displayStatus(str) {
    let status =  str.slice(1, str.length)
    if (!errStatus.includes(parseInt(status, 10))) return '404'
    else return status
}

const ErrorPage = (props) => (
    <div>
        <NavBar />
        <h1 className='err'>Error Status: {displayStatus(props.location.pathname)}</h1>
    </div>
)

export default withRouter(ErrorPage)