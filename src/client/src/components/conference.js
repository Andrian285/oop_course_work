import React from 'react'
import Paper from 'material-ui/Paper'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import FlatButton from 'material-ui/FlatButton'
import fetchData from '../utils/fetcher'
import LocalStorageManager from '../utils/localstorageManager'
import { errStatus } from '../utils/errorStatuses'
import { setConferences } from '../actions/conferenceActions'
import '../stylesheets/conference.css'

const backStyle = {
    margin: 'auto',
    maxWidth: 700,
    padding: '10px',
    textAlign: 'center',
    cursor: 'pointer'
}

async function handleClick(e, id, conferences, setConferences) {
    e.stopPropagation()
    let responseData = await fetchData({id}, '/conference/remove', 'post', LocalStorageManager.getToken())
    if (!errStatus.includes(responseData.status)) {
        let index = conferences.findIndex(x => x._id === id)
        let conferencesCopy = conferences.slice()
        conferencesCopy.splice(index, 1)
        setConferences(conferencesCopy)
    }
}

const App = (props) => (
    <Paper style={backStyle} zDepth={2} onClick={() => props.history.push('conferences/' + props.id)}>
        <i className='basis titlePaper'>{props.name}</i><br />
        <i className='basis datePaper'>Deadline: {props.date}</i><br />
        {props.personalConference && <FlatButton icon={<i className="material-icons" style={{ color: "red" }}>delete</i>} 
            onClick={(e) => handleClick(e, props.id, props.conferences, props.setConferences)} />}
    </Paper>
)

const mapStateToProps = (state) => ({
    conferences: state.conferences
})

const mapDispatchToProps = {
    setConferences
}
const Conference = connect(mapStateToProps, mapDispatchToProps)(App)

export default withRouter(Conference)