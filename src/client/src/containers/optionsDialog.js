import React from 'react'
import FlatButton from 'material-ui/FlatButton'
import Dialog from 'material-ui/Dialog'
import fetchData from '../utils/fetcher'
import LocalStorageManager from '../utils/localstorageManager'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'
import { setOptionsModal } from '../actions/navbarActions'
import { setUpdateUser } from '../actions/userActions'
import { updateParticipationFee } from '../actions/conferenceActions'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { errStatus } from '../utils/errorStatuses'
import '../stylesheets/navbar.css'

const customContentStyle = {
    width: '500px'
}

const errTitleStyle = {
    color: 'red',
    fontSize: 20
}

class App extends React.Component {
    state = {
        errTitle: '',
        open: false,
        currency: ''
    }

    onCancel = () => {
        this.setState({ errors: {}, open: false, errTitle: '' })
    }

    async processReceivedData(data) {
        let responseData = await data.json()
        if (!errStatus.includes(data.status)) {
            this.props.setUpdateUser(true)
            this.props.updateParticipationFee(true)
            this.setState({ open: false, errTitle: '' })
        }
        else this.setState({ errTitle: responseData.message })
    }

    onSubmit = async () => {
        let responseData = await fetchData({ currency: this.state.currency }, '/auth/updateCurrency', 'post', LocalStorageManager.getToken())
        this.processReceivedData(responseData)
    }

    handleChange = (event) => {
        let data = this.state.data
        data[event.target.name] = event.target.value
        this.setState({ data })
        this.validate(event.target.name, event.target.value)
    }

    render() {
        if (this.props.openOptions) {
            this.setState({ open: true }, () => this.props.setOptionsModal(false))
        }

        if (!this.props.user) return <div />

        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onClick={this.onCancel}
            />,
            <FlatButton
                label="Apply"
                primary={true}
                onClick={this.onSubmit}
            />,
        ]

        return (
            <div>
                <Dialog
                    actions={actions}
                    modal={true}
                    contentStyle={customContentStyle}
                    open={this.state.open}
                >
                    <div style={{ textAlign: 'center', fontSize: 20 }}>
                        <h1 style={errTitleStyle}>{this.state.errTitle}</h1>
                        Full name: <i>{this.props.user.name}</i><br />
                        Email: <i>{this.props.user.email}</i><br />
                        Username: <i>{this.props.user.username}</i><br />
                        Role: <i>{this.props.user.role}</i><br />
                        <i style={{ textAlign: 'left' }}><SelectField floatingLabelText="Currency" value={this.state.currency}
                            onChange={(e, i, v) => this.setState({ currency: v })}>
                            <MenuItem value='dollar' primaryText="Dollar" />
                            <MenuItem value='euro' primaryText="Euro" />
                            <MenuItem value='uah' primaryText="UAH" />
                        </SelectField></i>
                    </div>
                </Dialog>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    openOptions: state.openOptions
})

const mapDispatchToProps = {
    setOptionsModal,
    setUpdateUser,
    updateParticipationFee
}
const CustomDialog = connect(mapStateToProps, mapDispatchToProps)(App)

export default withRouter(CustomDialog)