import React from 'react'
import NavBar from '../containers/navbar'
import fetchData from '../utils/fetcher'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'
import TextField from 'material-ui/TextField'
import Snackbar from 'material-ui/Snackbar'
import LocalStorageManager from '../utils/localstorageManager'
import CircularProgress from 'material-ui/CircularProgress'
import { errStatus } from '../utils/errorStatuses'
import { setConferences, updateParticipationFee } from '../actions/conferenceActions'
import { setUserId, setBudget } from '../actions/userActions'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { Card, CardActions } from 'material-ui/Card'
import '../stylesheets/conference.css'

const styles = {
    uploadButton: {
        verticalAlign: 'middle',
    },
    uploadInput: {
        cursor: 'pointer',
        position: 'absolute',
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        width: '100%',
        opacity: 0,
    },
    snackbar: {
        color: '#FF502B',
        fontWeight: 'bold'
    },
    snackbarSuccess: {
        color: '#0DE634',
        fontWeight: 'bold'
    }
}

class App extends React.Component {
    state = {
        conference: null,
        joinClicked: false,
        name: '',
        nameErr: '',
        theses: '',
        snackbarErr: '',
        snackbarSuccess: '',
        loading: false,
        myId: null,
        participationFee: ''
    }

    componentDidMount() {
        this.getConferenceFromDB()
        this.getParticipationFee()
    }

    async getParticipationFee() {
        let responseData = await fetchData(null, '/auth/getParticipationFee', 'get', LocalStorageManager.getToken())
        let data = await responseData.json()
        this.setState({ participationFee: data.fee })
    }

    async getId() {
        let responseData = await fetchData(null, '/auth/getId', 'get', LocalStorageManager.getToken())
        if (!errStatus.includes(responseData.status)) {
            let data = await responseData.json()
            this.setState({ myId: data.id })
        }
    }

    async getConferenceFromDB() {
        let responseData = await fetchData(null, '/conference/' + this.props.match.params.id, 'get')
        let data = await responseData.json()
        data.conference.informationalList = Buffer.from(data.conference.informationalList, 'base64').toString()
        this.setState({ conference: data.conference })
    }

    renderSections() {
        return this.state.conference.sections.map(section =>
            <li key={section}>{section}</li>)
    }

    loadFile = (evt) => {
        const reader = new FileReader()
        let file = evt.target.files[0]
        reader.onloadend = () => {
            if (file.size > 900000)
                return this.setState({ snackbarErr: 'Too large file!' })
            if (file.type !== 'application/pdf')
                return this.setState({ snackbarErr: 'Must be pdf file!' })
            this.setState({ theses: new Buffer(reader.result).toString('base64') })
        }
        try {
            reader.readAsDataURL(file)
        } catch (e) {
            //loading canceled
        }
    }

    handleNameChange = (event) => {
        let nameErr = ''
        if (!event.target.value.length) nameErr = 'This field is required'
        this.setState({ name: event.target.value, nameErr })
    }

    async submit() {
        if (!this.state.name)
            return this.setState({ nameErr: 'This field is required' })
        if (!this.state.theses)
            return this.setState({ snackbarErr: 'Theses must be uploaded!' })
        this.setState({ loading: true })
        let responseData = await fetchData({
            name: this.state.name,
            theses: this.state.theses
        }, '/conference/join/' + this.props.match.params.id, 'post', LocalStorageManager.getToken())
        this.setState({ loading: false })
        if (!errStatus.includes(responseData.status)) {
            responseData = await fetchData(null, '/auth/getUser', 'get', LocalStorageManager.getToken())
            let data = await responseData.json()
            this.props.setBudget(data.user.budget)
            this.setState({ snackbarSuccess: 'Operation has completed successfully' })
        }
        else this.setState({ snackbarErr: 'Something went wrong :(' })
    }

    render() {
        if (!this.props.authenticated && this.state.myId) {
            this.setState({ myId: '' })
        } else if (this.props.authenticated && !this.state.myId) {
            this.getId()
        }
        if (this.props.updateFee) {
            this.getParticipationFee()
            this.props.updateParticipationFee(false)
        }
        return (
            <div>
                <NavBar />
                <br />
                {this.state.conference &&
                    <div style={{ textAlign: 'center' }}>
                        <h1 className='basis title' style={{ textAlign: 'center' }}>{this.state.conference.name}</h1>
                        <h2 className='basis date'>Deadline: {this.state.conference.date}</h2>
                        <ol type="I" className='basis sections'>
                            {this.renderSections()}
                        </ol>
                        <a href={this.state.conference.informationalList} download={"informList"}>
                            <RaisedButton label="download informational list" style={{ margin: 12 }} />
                        </a>
                        {(this.props.authenticated && this.state.myId !== this.state.conference.organizer) &&
                            <Card style={{ margin: 'auto', maxWidth: 300 }}>
                                <i className='basis price'>Participation fee: {this.state.participationFee}</i>
                                {this.state.joinClicked &&
                                    <div>
                                        <TextField hintText="Name" errorText={this.state.nameErr} onChange={this.handleNameChange} />
                                        <RaisedButton
                                            label="theses"
                                            labelPosition="before"
                                            style={styles.uploadButton}
                                            containerElement="label"
                                        >
                                            <input type="file" style={styles.uploadInput} onChange={(e) => this.loadFile(e)} />
                                        </RaisedButton>
                                    </div>}
                                <CardActions>
                                    {this.state.loading ?
                                        <CircularProgress size={30} style={{ verticalAlign: 'middle' }} />
                                        :
                                        <FlatButton label="join" onClick={() => this.state.joinClicked ? this.submit() : this.setState({ joinClicked: true })} />
                                    }
                                </CardActions>
                            </Card>
                        }
                    </div>}
                <Snackbar contentStyle={styles.snackbar}
                    open={this.state.snackbarErr.length !== 0}
                    message={this.state.snackbarErr}
                    autoHideDuration={4000}
                    onRequestClose={() => this.setState({ snackbarErr: '' })}
                />
                <Snackbar contentStyle={styles.snackbarSuccess}
                    open={this.state.snackbarSuccess.length !== 0}
                    message={this.state.snackbarSuccess}
                    autoHideDuration={4000}
                    onRequestClose={() => this.setState({ snackbarSuccess: '' })}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    userId: state.userId,
    conferences: state.conferences,
    authenticated: state.authenticated,
    updateFee: state.updateFee
})

const mapDispatchToProps = {
    setUserId,
    setConferences,
    setBudget,
    updateParticipationFee
}
const Conferences = connect(mapStateToProps, mapDispatchToProps)(App)

export default withRouter(Conferences)