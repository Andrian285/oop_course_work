import React from 'react'
import AppBar from 'material-ui/AppBar'
import FlatButton from 'material-ui/FlatButton'
import Drawer from 'material-ui/Drawer'
import MenuItem from 'material-ui/MenuItem'
import AuthDialog from './authDialog'
import LocalStorageManager from '../utils/localstorageManager'
import fetchData from '../utils/fetcher'
import Divider from 'material-ui/Divider'
import OptionsDialog from './optionsDialog'
import { setLoginModal, setSignupModal, setOptionsModal } from '../actions/navbarActions'
import { setAuthenticated, setOrganizer, setUserId, setBudget, setUpdateUser } from '../actions/userActions'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { errStatus } from '../utils/errorStatuses'
import '../stylesheets/navbar.css'

const styles = {
    title: {
        textAlign: 'center'
    },
    rightMenuButtons: {
        color: 'white',
        fontWeight: 900
    },
    budget: {
        margin: 15
    }
}

class App extends React.Component {
    state = {
        open: false,
        dataLoaded: true,
        user: null
    }

    handleSideMenu = () => this.setState({ open: !this.state.open })

    async componentDidMount() {
        if (!LocalStorageManager.isUserAuthenticated()) return
        this.setState({ dataLoaded: false })
        let responseData = await fetchData(null, '/auth/getUser', 'get', LocalStorageManager.getToken())
        let err = errStatus.includes(responseData.status)
        if (responseData.status === 401) LocalStorageManager.deauthenticateUser()
        if (!err) {
            this.props.setAuthenticated(true)
            let data = await responseData.json()
            this.props.setOrganizer(data.user.role === 'Organizer')
            this.setState({ user: data.user })
            this.props.setBudget(data.user.budget)
        }
        this.setState({ dataLoaded: true })
    }

    async updateUser() {
        let responseData = await fetchData(null, '/auth/getUser', 'get', LocalStorageManager.getToken())
        let err = errStatus.includes(responseData.status)
        if (!err) {
            let data = await responseData.json()
            this.setState({ user: data.user })
            this.props.setBudget(data.user.budget)
        }
        if (this.props.updateUser) this.props.setUpdateUser(false)
    }

    logout = () => {
        LocalStorageManager.deauthenticateUser()
        this.props.setAuthenticated(false)
        this.props.setOrganizer(false)
        this.props.setUserId('')
    }

    render() {
        if (this.props.updateUser) this.updateUser()
        return (
            <div style={{ position: 'sticky', top: 0, zIndex: '10' }}>
                <AppBar
                    title={this.props.location.pathname === '/' && <i className="material-icons" style={{ marginTop: '20px' }}>home</i>}
                    titleStyle={styles.title}
                    iconElementRight={
                        this.props.authenticated ?
                            <FlatButton label="Logout" style={styles.rightMenuButtons} onClick={this.logout} />
                            :
                            this.state.dataLoaded ?
                                <div style={{ marginTop: '6px' }}>
                                    <FlatButton label="Login" style={styles.rightMenuButtons} onClick={() => this.props.setLoginModal(true)} />
                                    <FlatButton label="Signup" style={styles.rightMenuButtons} onClick={() => this.props.setSignupModal(true)} />
                                </div>
                                :
                                <div />
                    }
                    onLeftIconButtonClick={this.handleSideMenu}
                />
                <Drawer open={this.state.open} onRequestChange={(open) => this.setState({ open })} docked={false}>
                    <MenuItem onClick={() => this.props.history.push('/')}>Home</MenuItem>
                    <MenuItem onClick={() => this.props.history.push('/conferences')}>Conferences</MenuItem>
                    {this.props.authenticated &&
                        <div>
                            <Divider />
                            <div style={styles.budget}>Budget: {this.props.budget}</div>
                            <MenuItem onClick={() => this.props.setOptionsModal(true)}>Options</MenuItem>
                        </div>}
                </Drawer>
                <AuthDialog />
                <OptionsDialog user={this.state.user} />
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    openSignup: state.openSignup,
    openLogin: state.openLogin,
    authenticated: state.authenticated,
    userId: state.userId,
    budget: state.budget,
    updateUser: state.updateUser
})

const mapDispatchToProps = {
    setLoginModal,
    setSignupModal,
    setAuthenticated,
    setOrganizer,
    setUserId,
    setOptionsModal,
    setBudget,
    setUpdateUser
}
const NavBar = connect(mapStateToProps, mapDispatchToProps)(App)

export default withRouter(NavBar)