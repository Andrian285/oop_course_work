import React from 'react'
import FlatButton from 'material-ui/FlatButton'
import TextField from 'material-ui/TextField'
import Dialog from 'material-ui/Dialog'
import fetchData from '../utils/fetcher'
import LocalStorageManager from '../utils/localstorageManager'
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton'
import { setLoginModal, setSignupModal } from '../actions/navbarActions'
import { setAuthenticated, setOrganizer, setUserId, setBudget, setUpdateUser } from '../actions/userActions'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { loginData, signupData } from '../utils/authModalData'
import { errStatus } from '../utils/errorStatuses'
import '../stylesheets/navbar.css'

const customContentStyle = {
    width: '25%',
    top: 0,
    right: 0,
    position: 'absolute'
}

const errTitleStyle = {
    color: 'red',
    fontSize: 20
}

class App extends React.Component {
    state = {
        errTitle: '',
        open: false,
        info: loginData,
        data: {},
        errors: {}
    }

    onCancel = () => {
        this.setState({ errors: {}, open: false, errTitle: '' })
    }

    async processReceivedData(data) {
        let responseData = await data.json()
        if (!errStatus.includes(data.status)) {
            if (responseData.token) {
                LocalStorageManager.authenticateUser(responseData.token)
                this.props.setAuthenticated(true)
                this.props.setOrganizer(responseData.user.role === 'Organizer')
                this.props.setUserId(responseData.user._id)
                this.props.setBudget(responseData.user.budget)
                this.props.setUpdateUser(true)
                this.setState({ open: false, errTitle: '' })
            }
            else this.setState({ open: true, info: loginData, errTitle: '' })
        }
        else this.setState({ errTitle: responseData.message })
    }

    validate(name, value) {
        if (name === 'role') return true
        let errors = this.state.errors, state = true
        if (name === 'confirm') {
            if (this.state.data.password !== value) {
                errors.confirm = this.state.info.confirm.error
                state = false
            }
            else errors.confirm = ''
        } else {
            if (!value || !this.state.info[name].validation(value)) {
                errors[name] = this.state.info[name].error
                state = false
            } else errors[name] = ''
        }
        this.setState({ errors })
        return state
    }

    onSubmit = async () => {
        let data = this.state.data
        if (!data.role) data.role = 'Participant'

        let state = true
        if ((this.state.info.name.state && !this.validate('name', data.name)) ||
            (this.state.info.username.state && !this.validate('username', data.username)) ||
            (this.state.info.email.state && !this.validate('email', data.email)) ||
            (this.state.info.password.state && !this.validate('password', data.password)) ||
            (this.state.info.confirm.state && !this.validate('confirm', data.confirm))) state = false
        if (!state) return
        let responseData = await fetchData(data, '/auth/' + this.state.info.content, 'post')
        this.processReceivedData(responseData)
    }

    handleChange = (event) => {
        let data = this.state.data
        data[event.target.name] = event.target.value
        this.setState({ data })
        this.validate(event.target.name, event.target.value)
    }

    renderElements() {
        let components = []
        if (this.state.info.name.state) components.push(
            <TextField name="name" key="name" onChange={this.handleChange}
                hintText="Full name" errorText={this.state.errors.name}
            />)
        if (this.state.info.username.state) components.push(
            <TextField name="username" key="username" onChange={this.handleChange}
                hintText="Username" errorText={this.state.errors.username}
            />)
        if (this.state.info.email.state) components.push(
            <TextField name="email" key="email" onChange={this.handleChange}
                hintText="Email" errorText={this.state.errors.email}
            />)
        if (this.state.info.password.state) components.push(
            <TextField name="password" key="pass" onChange={this.handleChange}
                hintText="Password" errorText={this.state.errors.password} type='password'
            />)
        if (this.state.info.confirm.state) components.push(
            <TextField name="confirm" key="confirm" onChange={this.handleChange}
                hintText="Conrirm password" errorText={this.state.errors.confirm} type='password'
            />)
        if (this.state.info.role) components.push(
            <div key="role">
                <br />
                <RadioButtonGroup name="role" defaultSelected="Participant" onChange={this.handleChange}>
                    <RadioButton style={{ marginBottom: 16 }}
                        value="Participant"
                        label="Participant"
                    />
                    <RadioButton style={{ marginBottom: 16 }}
                        value="Organizer"
                        label="Organizer"
                    />
                </RadioButtonGroup>
            </div>)
        return components
    }

    render() {
        if (this.props.openLogin) {
            this.setState({ open: true, info: loginData }, () => this.props.setLoginModal(false))
        } else if (this.props.openSignup) {
            this.setState({ open: true, info: signupData }, () => this.props.setSignupModal(false))
        }

        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onClick={this.onCancel}
            />,
            <FlatButton
                label="Submit"
                primary={true}
                onClick={this.onSubmit}
            />,
        ]

        return (
            <div>
                <Dialog
                    actions={actions}
                    modal={true}
                    contentStyle={customContentStyle}
                    open={this.state.open}
                >
                    <h1 style={errTitleStyle}>{this.state.errTitle}</h1>
                    {this.renderElements()}
                </Dialog>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    openSignup: state.openSignup,
    openLogin: state.openLogin
})

const mapDispatchToProps = {
    setLoginModal,
    setSignupModal,
    setAuthenticated,
    setOrganizer,
    setUserId,
    setBudget,
    setUpdateUser
}
const CustomDialog = connect(mapStateToProps, mapDispatchToProps)(App)

export default withRouter(CustomDialog)