import React from 'react'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'
import Dialog from 'material-ui/Dialog'
import fetchData from '../utils/fetcher'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import DatePicker from 'material-ui/DatePicker'
import Snackbar from 'material-ui/Snackbar'
import LocalStorageManager from '../utils/localstorageManager'
import CircularProgress from 'material-ui/CircularProgress'
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { errStatus } from '../utils/errorStatuses'
import { setLoginModal, setSignupModal } from '../actions/navbarActions'
import { setConferenceModal, setRefreshConferences } from '../actions/conferenceActions'
import '../stylesheets/navbar.css'

const styles = {
    content: {
        minWidth: '50%'
    },
    errTitle: {
        color: 'red',
        fontSize: 20
    },
    addSection: {
        verticalAlign: 'middle'
    },
    uploadButton: {
        verticalAlign: 'middle',
    },
    uploadInput: {
        cursor: 'pointer',
        position: 'absolute',
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        width: '100%',
        opacity: 0,
    },
    radioButton: {
        marginBottom: 16,
        textAlign: 'left'
    },
    dialog: {
        textAlign: 'center'
    },
    snackbar: {
        color: '#FF502B',
        fontWeight: 'bold'
    },
    radioButtonGroup: {
        margin: 'auto',
        width: 265
    }
}

class App extends React.Component {
    state = {
        errTitle: '',
        open: false,
        data: {},
        errors: {},
        sectionsAmount: 1,
        snackbarErr: '',
        loading: false
    }

    onCancel = () => {
        this.setState({ errors: {}, sectionsAmount: 1, open: false, errTitle: '', data: {} })
    }

    async processReceivedData(data) {
        let responseData = await data.json()
        if (!errStatus.includes(data.status)) {
            this.setState({ loading: false, errors: {}, sectionsAmount: 1, open: false, errTitle: '', data: {} })
            this.props.setRefreshConferences(true)
        }
        else this.setState({ errTitle: responseData.message })
    }

    validate(name, value) {
        let errors = this.state.errors
        if (!value) {
            errors[name] = 'This field is required'
            this.setState({ errors })
            return false
        }
        errors[name] = ''
        this.setState({ errors })
        return true
    }

    onSubmit = async () => {
        let data = this.state.data
        if (!this.validate('name', data.name)) return
        if (data.type === 'distant' && !this.validate('date', data.date)) return
        if (!data.type) data.type = 'full-time'
        if (!data.informationalList) return this.setState({ snackbarErr: 'Informational list is required field!' })
        for (let index = 0; index < this.state.sectionsAmount; index++) {
            if (!data.sections || !data.sections[index]) return this.setState({ snackbarErr: 'All sections must been filled!' })
        }
        this.setState({ loading: true })
        let responseData = await fetchData(this.state.data, '/conference/create', 'post', LocalStorageManager.getToken())
        this.processReceivedData(responseData)
    }

    handleChange = (event, value) => {
        if (!event) {
            let data = this.state.data
            data['date'] = value
            return this.setState({ data })
        }
        if (event.target.name === 'type') {
            let data = this.state.data
            data['type'] = value
            return this.setState({ data })
        }
        if (this.validate(event.target.name, value)) {
            let data = this.state.data
            if (event.target.name === 'name') data[event.target.name] = value
            else {
                if (!data.sections) data.sections = []
                let index = event.target.name.match(/\d/g).join("")
                data.sections[index] = value
            }
            this.setState({ data })
        }
    }

    loadFile = async (evt) => {
        const reader = new FileReader()
        let file = evt.target.files[0]
        reader.onloadend = () => {
            if (file.size > 900000) {
                return this.setState({ snackbarErr: 'Too large file!' })
            }
            else {
                let data = this.state.data
                data.informationalList = new Buffer(reader.result).toString('base64')
                this.setState({ data })
            }
        }
        try {
            reader.readAsDataURL(file)
        } catch (e) {
            //loading canceled
        }
    }

    renderSections() {
        let sections = []
        for (let i = 0; i < this.state.sectionsAmount; i++) sections.push(
            <TextField name={"section" + i} hintText="Section" key={i} onChange={this.handleChange} />
        )
        return sections
    }

    render() {
        if (this.props.openConferenceModal) {
            this.setState({ open: true }, () => this.props.setConferenceModal(false))
        }

        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onClick={this.onCancel}
            />,
            this.state.loading ?
                <CircularProgress size={30} style={{ verticalAlign: 'middle' }} />
                :
                <FlatButton
                    label="Submit"
                    primary={true}
                    onClick={this.onSubmit}
                />
        ]

        return (
            <div>
                <Dialog style={styles.dialog}
                    actions={actions}
                    modal={true}
                    contentStyle={styles.content}
                    open={this.state.open}
                    autoScrollBodyContent={true}
                >
                    <h1 style={styles.errTitle}>{this.state.errTitle}</h1>
                    <TextField name="name" hintText="Name of the conference"
                        errorText={this.state.errors.name} onChange={this.handleChange}
                    /><br />
                    <RadioButtonGroup name="type" defaultSelected="full-time" onChange={this.handleChange} style={styles.radioButtonGroup} >
                        <RadioButton style={styles.radioButton}
                            value="full-time"
                            label="Full-time"
                        />
                        <RadioButton style={styles.radioButton}
                            value="distant"
                            label="Distant"
                        />
                    </RadioButtonGroup>
                    {this.state.data.type === 'distant' && <DatePicker hintText="Deadline" onChange={this.handleChange} errorText={this.state.errors.date} />}
                    <RaisedButton name="informationalList"
                        label="Choose an informational list"
                        labelStyle={{ fontWeight: 'bold' }}
                        labelPosition="before"
                        style={styles.uploadButton}
                        containerElement="label"
                    >
                        <input type="file" style={styles.uploadInput} onChange={this.loadFile} />
                    </RaisedButton>
                    <h2>Sections</h2>
                    {this.renderSections()}
                    <FloatingActionButton onClick={() => this.setState({ sectionsAmount: this.state.sectionsAmount + 1 })}
                        style={styles.addSection}>
                        <ContentAdd style={{ color: '#FB8C00' }} />
                    </FloatingActionButton>
                </Dialog>

                <Snackbar contentStyle={styles.snackbar}
                    open={this.state.snackbarErr.length !== 0}
                    message={this.state.snackbarErr}
                    autoHideDuration={4000}
                    onRequestClose={() => this.setState({ snackbarErr: '' })}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    openSignup: state.openSignup,
    openLogin: state.openLogin,
    openConferenceModal: state.openConferenceModal
})

const mapDispatchToProps = {
    setLoginModal,
    setSignupModal,
    setConferenceModal,
    setRefreshConferences
}
const CustomDialog = connect(mapStateToProps, mapDispatchToProps)(App)

export default withRouter(CustomDialog)