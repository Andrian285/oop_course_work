import React from 'react'
import NavBar from '../containers/navbar'
import SearchBar from 'material-ui-search-bar'
import Conference from '../components/conference'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import fetchData from '../utils/fetcher'
import ConferenceDialog from './conferenceDialog'
import { setConferenceModal, setRefreshConferences, setConferences } from '../actions/conferenceActions'
import { setUserId } from '../actions/userActions'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import LocalStorageManager from '../utils/localstorageManager'

class App extends React.Component {
    state = {
        search: ''
    }

    componentDidMount() {
        this.getConferencesFromDB()
    }

    async getConferencesFromDB() {
        let responseConferenceData = await fetchData(null, '/conference', 'get')
        let conferenceData = await responseConferenceData.json()

        let responseUserData = await fetchData(null, '/auth/getId', 'get', LocalStorageManager.getToken())
        let userData = await responseUserData.json()

        this.props.setUserId(userData.id)
        this.props.setConferences(conferenceData.conferences)
    }

    renderConferences() {
        if (this.props.conferences)
            return this.props.conferences.map((conference) =>
                <div key={conference.name}>
                    <Conference name={conference.name} date={conference.date}
                        id={conference._id} personalConference={conference.organizer === this.props.userId} /><br />
                </div>)
    }

    onSerch = () => {
        if (!this.props.conferences) return
        let conferences = this.props.conferences.filter(x => x.name.toLowerCase().includes(this.state.search.toLowerCase()))
        this.setState({ conferences })
    }

    render() {
        if (this.props.refreshConferences) {
            this.getConferencesFromDB()
            this.props.setRefreshConferences(false)
        }
        return (
            <div>
                <NavBar />
                <br />
                <SearchBar
                    onChange={(search) => this.setState({ search })}
                    onRequestSearch={this.onSerch}
                    style={{
                        margin: '0 auto',
                        maxWidth: 800
                    }}
                />
                <br />
                {this.props.isOrganizer &&
                    <FloatingActionButton style={{ marginLeft: '50%' }} onClick={() => this.props.setConferenceModal(true)}>
                        <ContentAdd />
                    </FloatingActionButton>}
                <ConferenceDialog />
                <br /><br />
                {this.renderConferences()}
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    isOrganizer: state.isOrganizer,
    refreshConferences: state.refreshConferences,
    userId: state.userId,
    conferences: state.conferences
})

const mapDispatchToProps = {
    setConferenceModal,
    setRefreshConferences,
    setUserId,
    setConferences
}
const Conferences = connect(mapStateToProps, mapDispatchToProps)(App)

export default withRouter(Conferences)