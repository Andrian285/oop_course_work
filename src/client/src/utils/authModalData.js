import validator from 'email-validator'

export const loginData = {
    content: 'login',
    name: {
        state: false,
        error: 'Incorrect Full name: name and surname must be from capital letters',
        validation: name => { return /^[A-Z]+[a-z]+\s+[A-Z]+[a-z]+$/.test(name) }
    },
    username: {
        state: false,
        error: 'Incorrect username: must be at least 3 characters',
        validation: username => { return username.length >= 3 }
    },
    email: {
        state: true,
        error: 'Incorrect e-mail',
        validation: email => { return validator.validate(email) }
    },
    password: {
        state: true,
        error: 'Incorrect password: must be at least 8 characters',
        validation: pass => { return pass.length >= 8 }
    },
    confirm: {
        state: false,
        error: 'The password and its confirm are not the same'
    },
    role: false
}

export const signupData = {
    content: 'signup',
    name: {
        state: true,
        error: 'Incorrect Full name: name and surname must be from capital letters',
        validation: name => { return /^[A-Z]+[a-z]+\s+[A-Z]+[a-z]+$/.test(name) }
    },
    username: {
        state: true,
        error: 'Incorrect username: must be at least 3 characters',
        validation: username => { return username.length >= 3 }
    },
    email: {
        state: true,
        error: 'Incorrect e-mail',
        validation: email => { return validator.validate(email) }
    },
    password: {
        state: true,
        error: 'Incorrect password: must be at least 8 characters',
        validation: pass => { return pass.length >= 8 }
    },
    confirm: {
        state: true,
        error: 'The password and its confirm are not the same'
    },
    role: true
}