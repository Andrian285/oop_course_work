import * as bodyParser from 'body-parser'
import * as busboyBodyParser from 'busboy-body-parser'
import * as express from 'express'
import * as passport from 'passport'
import { initPassport } from './authorization/initPassport'
import authRouter from './routes/auth'
import conferenceRouter from './routes/conference'

class App {
    public express

    constructor() {
        this.express = express()

        this.express.use(bodyParser.json({
            limit: '900kb'
        }))
        this.express.use(busboyBodyParser({ limit: '5mb' }))
        this.express.use(passport.initialize())

        initPassport()
        this.mountRoutes()
    }

    private mountRoutes(): void {
        this.express.use('/conference', conferenceRouter)
        this.express.use('/auth', authRouter)
        this.express.get('/*', (req, res) => {
            res.status(404).json({
                message: 'Not Found!'
            })
        })
        this.express.post('/*', (req, res) => {
            res.status(404).json({
                message: 'Not Found'
            })
        })
    }
}

export default new App().express
