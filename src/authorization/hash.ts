import * as crypto from 'crypto'

export default function sha512(password, salt) {
    const hash = crypto.createHmac('sha512', salt)
    hash.update(password)
    const value = hash.digest('hex')
    return {
        salt,
        passwordHash: value
    }
}
