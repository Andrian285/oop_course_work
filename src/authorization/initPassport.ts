import * as jwt from 'jsonwebtoken'
import * as passport from 'passport'
import * as Local from 'passport-local'
import { Config } from '../config'
import { UserProxy } from '../classes/proxy/userProxy'
import sha512 from './hash'

const LocalStrategy = Local.Strategy

export function initPassport() {
    passport.use(new LocalStrategy({
        session: false,
        usernameField: 'email'
    },
        async (email, password, done) => {
            const hash = sha512(password, Config.ServerSalt).passwordHash
            const up = new UserProxy()
            try {
                const user = await up.getUserByEmailAndPasshash(email, hash)
                const payload = {
                    sub: user._id
                }
                const token = jwt.sign(payload, Config.JwtSecret, {
                    expiresIn: 60 * 60 * 24 * 30
                })
                done(user ? null : false, token, user)
            } catch (err) {
                done('no user', null, null)
            }
        }
    ))
}
