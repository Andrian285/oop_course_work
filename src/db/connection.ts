import * as dotenv from 'dotenv'
import * as mongoose from 'mongoose'
dotenv.config()

mongoose.connect(process.env.MONGODB_URI)
