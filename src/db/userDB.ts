import * as mongoose from 'mongoose'
import User from '../models/user'

import './connection'

export function create(user) {
    const entity = new User(user)
    return entity.save()
}

export function getAll() {
    return User.find().exec()
}

export function getById(userId) {
    return User.findOne({ _id: userId }).exec()
}

export function update(userId, user) {
    const entity = new User(user)
    return User.findOneAndUpdate({ _id: userId }, entity)
}

export function remove(userId) {
    return User.remove({ _id: userId })
}

export function getUserByEmailAndPasshash(email, passHash) {
    return User.findOne({ email, passwordHash: passHash }).exec()
}

export async function isAlreadyExists(username, email) {
    const uname = await User.findOne({ username }).exec()
    const mail = await User.findOne({ email }).exec()
    if (!uname && !mail) return false
    return true
}

export function updatePhoto(userId, photo) {
    return User.update({ _id: userId }, { photo })
}
