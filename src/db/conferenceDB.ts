import * as mongoose from 'mongoose'
import Conference from '../models/conference'

import './connection'

export function create(conference) {
    const entity = new Conference(conference)
    return entity.save()
}

export function getAll() {
    return Conference.find().exec()
}

export function getById(conferenceId) {
    return Conference.findOne({ _id: conferenceId }).exec()
}

export function update(conferenceId, conference) {
    const entity = new Conference(conference)
    return Conference.findOneAndUpdate({ _id: conferenceId }, entity)
}

export function remove(conferenceId) {
    return Conference.remove({ _id: conferenceId })
}

export function isAlreadyExists(name) {
    return Conference.findOne({name}).exec()
}

export function searchByName(name, userId) {
    return Conference.find({header: new RegExp(name, 'i'), userId}).exec()
}
