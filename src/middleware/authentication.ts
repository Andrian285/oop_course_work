import * as jwt from 'jsonwebtoken'
import { UserProxy } from '../classes/proxy/userProxy'
import { Config } from '../config'
import { getAll } from '../db/userDB'

export function authentication(req, res, next) {
    function unauthorized() {
        res.status(401).json({ message: 'Unauthorized' })
    }

    if (!req.headers.token) return unauthorized()

    return jwt.verify(req.headers.token, Config.JwtSecret, async (err, decoded) => {
        if (err) return unauthorized()

        const userId = decoded.sub

        const userProxy = new UserProxy()
        try {
            const user = await userProxy.getById(userId)
            if (!user) return unauthorized()
            res.locals.user = user
            next()
        } catch (e) { return res.status(500).json({ message: 'Internal Server Error' }) }
    })
}
