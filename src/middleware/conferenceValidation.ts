export function conferenceValidation(req, res, next) {
    function invalidArgs() {
        res.status(400).json({message: 'Invalid arguments'})
    }

    if (req.body.type === 'distant' && !req.body.date) return invalidArgs()
    if (!req.body.name || !req.body.sections || !req.body.informationalList) return invalidArgs()
    if (!(req.body.sections instanceof Array) || req.body.sections.length === 0) return invalidArgs()
    next()
}
