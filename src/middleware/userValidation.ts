export function userValidation(req, res, next) {
    function invalidArgs() {
        res.status(400).json({ message: 'Invalid arguments' })
    }

    if (!req.body.name || !req.body.username || !req.body.password
        || !req.body.email) return invalidArgs()
    if (req.body.role !== 'Participant' && req.body.role !== 'Organizer') return invalidArgs()
    if (req.body.password.length < 8) return invalidArgs()
    next()
}

export function loginValidation(req, res, next) {
    if (!req.body.email || !req.body.password) return res.status(400).json({ message: 'Invalid arguments' })
    next()
}
