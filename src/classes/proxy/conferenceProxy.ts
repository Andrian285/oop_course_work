import { create, getAll, getById, isAlreadyExists, remove } from '../../db/conferenceDB'
import { isBase64 } from '../deserializer/deserializer'
import StatusError from '../error/error'

export class ConferenceProxy {
    private conference

    public setConference(conference) {
        this.conference = conference
    }

    public async sendToDB() {
        if (this.correctDate() && await this.correctName() && this.correctOrganizer()
            && this.correctSections() && isBase64(this.conference.informationalList)) {
            await create(this.conference)
        } else throw new StatusError(400, 'Conference proxy has caught invalid arguments!')
    }

    public async getAll() {
        const conferences = await getAll()
        return JSON.parse(JSON.stringify(conferences))
    }

    public async getById(id) {
        const conference = await getById(id)
        return JSON.parse(JSON.stringify(conference))
    }

    public async remove(id: string, user) {
        const conference = await this.getById(id)
        if (user.role !== 'Organizer' || user._id.toString() !== conference.organizer)
            throw new StatusError(403, 'Forbidden')
        await remove(id)
    }

    public correctDate(): boolean {
        return !isNaN(Date.parse(this.conference.date))
    }

    public async correctName(): Promise<boolean> {
        return ((typeof this.conference.name === 'string') && this.conference.name.length > 0
            && !(await isAlreadyExists(this.conference.name)))
    }

    public correctOrganizer(): boolean {
        return ((typeof this.conference.organizer === 'string') && this.conference.organizer.match(/^[0-9a-fA-F]{24}$/))
    }

    public correctSections(): boolean {
        return ((this.conference.sections instanceof Array) && this.conference.sections.length > 0)
    }
}
