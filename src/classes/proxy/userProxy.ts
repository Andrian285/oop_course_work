import * as emailValidator from 'email-validator'
import { create, getById, isAlreadyExists, update, getUserByEmailAndPasshash, getAll } from '../../db/userDB'
import StatusError from '../error/error'

export class UserProxy {
    private user

    public setUser(user) {
        this.user = user
    }

    public async isAlreadyExists(username: string, email: string): Promise<boolean> {
        return isAlreadyExists(username, email)
    }

    public async create() {
        if (this.correctUser()) {
            await create(this.user)
        } else throw new StatusError(400, 'User proxy has cught invalid Arguments')
    }

    public async getById(userId: string) {
        const user = await getById(userId)
        return JSON.parse(JSON.stringify(user))
    }

    public async getAll() {
        const users = await getAll()
        return JSON.parse(JSON.stringify(users))
    }

    public async getUserByEmailAndPasshash(email, passHash) {
        const user = await getUserByEmailAndPasshash(email, passHash)
        return JSON.parse(JSON.stringify(user))
    }

    public update(userId) {
        if (this.correctUser()) 
            return update(userId, this.user)
        else throw new StatusError(400, 'User proxy has cught invalid Arguments')
    }

    public correctUser(): boolean {
        return this.correctName() && this.correctUsername() && this.correctEmail()
    }

    public correctName(): boolean {
        return (typeof this.user.name === 'string' && /^[A-Z]+[a-z]+\s+[A-Z]+[a-z]+$/.test(this.user.name))
    }

    public correctUsername(): boolean {
        return (typeof this.user.username === 'string' && this.user.username.length > 3)
    }

    public correctEmail(): boolean {
        return emailValidator.validate(this.user.email)
    }
}
