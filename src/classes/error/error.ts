export default class StatusError extends Error {
    public status: number

    constructor(status = 500, ...params) {
        super(...params)
        this.status = status
    }
}
