import { ConferenceProxy } from '../proxy/conferenceProxy'
import { Conference } from './conference'

export default class Director {
    public async generateConference(conference: Conference, userId: string, cp: ConferenceProxy, req) {
        conference.setName(req.body.name)
        conference.setDate(req.body.date)
        conference.setOrganizer(userId)
        conference.setSections(req.body.sections)
        conference.setInformationalList(req.body.informationalList)
        await conference.submit(cp)
    }
}

// binary test:
// 010010000110010101101100011011000110111100100000010000100110100101
// 1011100110000101110010011110010010000001010111011011110111001001101100011001000010000000100001

// base64 test:
// SGVsbG8gYmFzZTY0IHdvcmxk

// uri test:
// https://mozilla.org/?x=%D1%88%D0%B5%D0%BB%D0%BB%D1%8B
