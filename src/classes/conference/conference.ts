import { Deserializer } from '../deserializer/deserializer'
import { ConferenceProxy } from '../proxy/conferenceProxy'

export abstract class Conference {
    protected date: Date
    protected name: string
    protected organizer: string
    protected sections: string[]
    protected informationalList: string

    public setOrganizer(userId: string) {
        this.organizer = userId
    }
    public setSections(sections: string[]) {
        this.sections = sections.slice()
    }
    public setInformationalList(list: string) {
        const deserializer = new Deserializer()
        this.informationalList = deserializer.deserialize(list)
    }
    public setDate(date: Date) { }
    public setName(name: string) { }

    public async submit(cp: ConferenceProxy) {
        const model = {
            date: this.date,
            name: this.name,
            organizer: this.organizer,
            sections: this.sections,
            informationalList: this.informationalList
        }
        cp.setConference(JSON.parse(JSON.stringify(model)))
        await cp.sendToDB()
    }
}

export class FulltimeConference extends Conference {
    public setDate() {
        const date = new Date()
        date.setMonth(date.getMonth() + 6)
        this.date = date
    }

    public setName(name) {
        this.name = 'Full-time conference: ' + name
    }
}

export class DistantConference extends Conference {
    public setDate(date: Date) {
        this.date = date
    }

    public setName(name) {
        this.name = 'Distant conference: ' + name
    }
}
