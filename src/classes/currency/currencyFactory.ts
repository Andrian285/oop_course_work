import StatusError from '../error/error'
import {Dollar, Euro, UAH, Currency} from './currency'

export class CurrencyFactory {
    createCurrency(currency: string): Currency {
        switch (currency) {
            case 'dollar':
                return new Dollar()
            case 'euro':
                return new Euro()
            case 'uah':
                return new UAH()
            default:
                throw new StatusError(400, 'Invalid currency')
        }
    }
}
