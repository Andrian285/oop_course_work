import { UserProxy } from '../proxy/userProxy'

//i may be fucking insane and get exchange rate from api
export const exchangeRate = {
    dollar: 26,
    euro: 32
}

function round(number, precision) {
    var shift = function (number, precision, reverseShift) {
        if (reverseShift) {
            precision = -precision
        }
        var numArray = ("" + number).split("e")
        return +(numArray[0] + "e" + (numArray[1] ? (+numArray[1] + precision) : precision))
    }
    return shift(Math.round(shift(number, precision, false)), precision, true)
}

export abstract class Currency {
    protected budget: number
    protected centralized: number
    protected symbol: string

    public setCurrentBudget(budget: number) {
        this.setBudget(budget)
        this.setSymbol()
    }

    public getCurrentBudget() {
        return this.budget + this.symbol
    }

    public async save(user, currency) {
        const up = new UserProxy()
        user.centralizedBudget = this.centralized
        user.currency = currency
        up.setUser(user)
        await up.update(user._id)
    }

    public setSymbol() { }
    public setBudget(budget: number) { }
}

export class Dollar extends Currency {
    public setBudget(budget: number) {
        this.budget = round(budget / exchangeRate.dollar, 2)
        this.centralized = budget
    }

    public setSymbol() {
        this.symbol = '$'
    }
}

export class Euro extends Currency {
    public setBudget(budget: number) {
        this.budget = round(budget / exchangeRate.euro, 2)
        this.centralized = budget
    }

    public setSymbol() {
        this.symbol = '€'
    }
}

export class UAH extends Currency {
    public setBudget(budget: number) {
        this.budget = round(budget, 2)
        this.centralized = budget
    }

    public setSymbol() {
        this.symbol = '₴'
    }
}