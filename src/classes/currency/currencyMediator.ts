import { UserProxy } from '../proxy/userProxy'
import StatusError from '../error/error'
import { Config } from '../../config';
import { CurrencyFactory } from './currencyFactory'
import { Currency } from './currency'

export class Transactor {
    private payers: Payer[]

    constructor() {
        this.payers = []
    }

    public register(payer: Payer) {
        this.payers.push(payer)
    }

    public async pay(to: string) {
        let receiver: Payer
        for (const payer of this.payers)
            if (payer.getUserId() === to) {
                receiver = payer
                break
            }
        if (!receiver) throw new StatusError(400, 'Invalid sender or receiver id!')
        await receiver.getPaid()
    }
}

export class Payer {
    private transactor: Transactor
    private currency: Currency
    private user

    constructor(user, transactor: Transactor) {
        this.user = user
        const cf = new CurrencyFactory()
        this.currency = cf.createCurrency(this.user.currency)
        this.transactor = transactor
    }

    public getUserId(): string {
        return this.user._id
    }

    public async pay(to: string) {
        this.currency.setBudget(this.user.centralizedBudget - Config.ParticipationFee)
        await this.currency.save(this.user, this.user.currency)
        await this.transactor.pay(to)
    }

    public async getPaid() {
        this.currency.setBudget(parseFloat(this.user.centralizedBudget) + Config.ParticipationFee)
        await this.currency.save(this.user, this.user.currency)
    }
}