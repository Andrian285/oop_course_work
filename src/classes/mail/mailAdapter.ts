import StatusError from '../error/error'
import MailAdaptee from './mailAdaptee'

interface IMail {
    sendMail()
}

export default class MailAdapter implements IMail {
    private message
    private mailAdaptee: MailAdaptee

    constructor(message) {
        this.message = message
        this.mailAdaptee = new MailAdaptee(message.from)
    }

    public sendMail() {
        return new Promise((resolve, reject) => {
            this.mailAdaptee.createTestAccount((err, account) => {
                if (err) reject(new StatusError(500, err))
                else {
                    const transporter = this.mailAdaptee.createTransport(account)
                    this.mailAdaptee.sendMail(transporter, this.message, sendErr => {
                        if (sendErr) reject(new StatusError(500, sendErr))
                        else resolve('Success')
                    })
                }
            })
        })
    }
}
