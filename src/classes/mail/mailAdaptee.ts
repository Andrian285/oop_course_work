import * as nodeMailer from 'nodemailer'

export default class MailAdaptee {
    private senderEmail: string

    public constructor(senderEmail: string) {
        this.senderEmail = senderEmail
    }

    public createTestAccount(cb) {
        nodeMailer.createTestAccount(async (err, account) => {
            if (err) {
                return cb('Failed to create a testing account')
            }

            cb(null, account)
        })
    }

    public sendMail(transporter, message, cb) {
        transporter.sendMail(message, (error, info) => {
            if (error) {
                console.log('Error occurred')
                console.log(error.message)
                throw cb('Error occured')
            }

            console.log('Message sent successfully!')
            console.log(nodeMailer.getTestMessageUrl(info))

            // only needed when using pooled connections
            transporter.close()
            cb()
        })
    }

    public getMessageURL(info) {
        return nodeMailer.getTestMessageUrl(info)
    }

    public createTransport(account) {
        return nodeMailer.createTransport(
            {
                host: account.smtp.host,
                port: account.smtp.port,
                secure: account.smtp.secure,
                auth: {
                    user: account.user,
                    pass: account.pass
                },
                logger: false,
                debug: false
            },
            {
                from: this.senderEmail
            }
        )
    }
}
