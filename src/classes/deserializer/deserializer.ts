import StatusError from '../error/error'

function unableToDeserialize() {
    throw new StatusError(400, 'Unable to deserialize')
}

function btoa(str) {
    return Buffer.from(str).toString('base64')
}

function atob(encoded) {
    return Buffer.from(encoded, 'base64').toString()
}

interface IDeserializer {
    deserialize(data: string)
}

class BinaryDeserializer implements IDeserializer {
    public successor: IDeserializer

    public deserialize(str: string) {
        if (!/^[01\s]*$/.test(str)) {
            if (this.successor) return this.successor.deserialize(str)
            else unableToDeserialize()
        }

        // Removes the spaces from the binary string
        str = str.replace(/\s+/g, '')
        // Pretty (correct) print binary (add a space every 8 characters)
        str = str.match(/.{1,8}/g).join(' ')

        const newBinary = str.split(' ')
        const binaryCode = []

        let i
        for (i = 0; i < newBinary.length; i++) {
            binaryCode.push(String.fromCharCode(parseInt(newBinary[i], 2)))
        }

        // console.log(binaryCode.join(''))
        return btoa(binaryCode.join(''))
    }
}

class Base64Deserializer implements IDeserializer {
    public successor: IDeserializer

    public deserialize(data: string) {
        if (!isBase64(data)) {
            if (this.successor) return this.successor.deserialize(data)
            else unableToDeserialize()
        }
        // console.log(this.atob(data))
        return data
    }
}

class URIDeserializer implements IDeserializer {
    public successor: IDeserializer

    public deserialize(data: string) {
        try {
            // console.log(decodeURI(data))
            return btoa(decodeURI(data))
        } catch (err) {
            if (this.successor) this.successor.deserialize(data)
            else unableToDeserialize()
        }
    }
}

export function isBase64(str): boolean {
    try {
        return btoa(atob(str)) === str
    } catch (err) {
        return false
    }
}

export class Deserializer {
    public deserialize(data: string): string {
        const binaryDeserializer = new BinaryDeserializer()
        const base64Deserializer = new Base64Deserializer()
        const uriDeserializer = new URIDeserializer()
        base64Deserializer.successor = binaryDeserializer
        binaryDeserializer.successor = uriDeserializer

        return base64Deserializer.deserialize(data)
    }
}
