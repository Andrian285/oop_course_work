import * as express from 'express'
import * as passport from 'passport'
import sha512 from '../authorization/hash'
import { UserProxy } from '../classes/proxy/userProxy'
import { Config } from '../config';
import { authentication } from '../middleware/authentication'
import { loginValidation, userValidation } from '../middleware/userValidation'
import { CurrencyFactory } from '../classes/currency/currencyFactory'

const router = express.Router()

function convertBudget(user) {
    let cf = new CurrencyFactory()
    let currency = cf.createCurrency(user.currency)
    currency.setCurrentBudget(user.centralizedBudget)
    delete (user.currency)
    delete (user.centralizedBudget)
    user.budget = currency.getCurrentBudget()
}

router.post('/signup', userValidation, async (req, res) => {
    const user = {
        name: req.body.name,
        username: req.body.username,
        passwordHash: sha512(req.body.password, Config.ServerSalt).passwordHash,
        email: req.body.email,
        role: req.body.role,
        centralizedBudget: Config.Budget,
        currency: Config.Currency
    }
    try {
        const userProxy = new UserProxy()
        if (await userProxy.isAlreadyExists(req.body.username, req.body.email))
            return res.status(400).json({ message: 'Such user is already exists' })
        userProxy.setUser(user)
        await userProxy.create()
        return res.json({ message: 'Success' })
    } catch (err) {
        return res.status(err.status).json({ message: err.message })
    }
})

router.post('/login', loginValidation, (req, res, next) => {
    passport.authenticate('local', (err, token, userData) => {
        if (err) return res.status(400).json({
            message: 'Incorrect email or password'
        })
        convertBudget(userData)
        return res.json({
            token,
            user: userData
        })
    })(req, res, next)
})

router.get('/authenticate', authentication, (req, res) => {
    res.json({ message: 'Success', user: res.locals.user })
})

router.get('/getUser', authentication, (req, res) => {
    try {
        convertBudget(res.locals.user)
        res.json({ message: 'Success', user: res.locals.user })
    } catch (err) {
        return res.status(err.status).json({ message: err.message })
    }
})

router.get('/getParticipationFee', authentication, (req, res) => {
    const cf = new CurrencyFactory()
    const currency = cf.createCurrency(res.locals.user.currency)
    currency.setCurrentBudget(Config.ParticipationFee)
    res.json({ fee: currency.getCurrentBudget() })
})

router.post('/updateCurrency', authentication, async (req, res) => {
    try {
        const cf = new CurrencyFactory()
        const currency = cf.createCurrency(req.body.currency)
        currency.setBudget(res.locals.user.centralizedBudget)
        await currency.save(res.locals.user, req.body.currency)
        res.json({ message: 'Success' })
    } catch (err) {
        return res.status(err.status).json({ message: err.message })
    }
})

router.get('/getId', authentication, (req, res) => {
    res.json({ id: res.locals.user._id })
})

export default router
