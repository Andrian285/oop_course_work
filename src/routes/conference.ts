import * as Email from 'email-templates'
import * as express from 'express'
import * as nodeMailer from 'nodemailer'
import { DistantConference, FulltimeConference } from '../classes/conference/conference'
import Director from '../classes/conference/director'
import { Deserializer } from '../classes/deserializer/deserializer'
import MailAdapter from '../classes/mail/mailAdapter'
import { ConferenceProxy } from '../classes/proxy/conferenceProxy'
import { UserProxy } from '../classes/proxy/userProxy'
import { authentication } from '../middleware/authentication'
import { conferenceValidation } from '../middleware/conferenceValidation'
import { Transactor, Payer } from '../classes/currency/currencyMediator'

function dateToString(date: Date): string {
    const months = ['January', 'Februrary', 'March', 'April', 'May', 'June', 'July', 'August',
        'September', 'October', 'November', 'December']
    const year = date.getFullYear()
    const month = months[date.getMonth()]
    return year + '/' + month + '/' + date.getDate()
}

const router = express.Router()

router.get('/', async (req, res) => {
    try {
        const cp = new ConferenceProxy()
        const conferences = await cp.getAll()

        for (const index in conferences)
            conferences[index].date = dateToString(new Date(conferences[index].date))
        return res.json({ conferences })
    } catch (err) {
        return res.status(err.status).json({ message: err.message })
    }
})

router.get('/:id', async (req, res) => {
    try {
        const cp = new ConferenceProxy()
        const conference = await cp.getById(req.params.id)

        conference.date = dateToString(new Date(conference.date))
        return res.json({ conference })
    } catch (err) {
        return res.status(err.status).json({ message: err.message })
    }
})

router.post('/create', authentication, conferenceValidation, async (req, res) => {
    if (res.locals.user.role !== 'Organizer') return res.status(403).json({ message: 'Forbidden' })
    try {
        const director = new Director()
        const cp = new ConferenceProxy()
        if (req.body.type === 'full-time') {
            const conference = new FulltimeConference()
            await director.generateConference(conference, res.locals.user._id, cp, req)
        } else if (req.body.type === 'distant') {
            const conference = new DistantConference()
            await director.generateConference(conference, res.locals.user._id, cp, req)
        } else return res.status(400).json({ message: 'Invalid conference type!' })
        return res.json({ message: 'Success' })
    } catch (err) {
        return res.status(err.status).json({ message: err.message })
    }
})

router.post('/remove', authentication, async (req, res) => {
    try {
        const cp = new ConferenceProxy()
        await cp.remove(req.body.id, res.locals.user)
        return res.json({ message: 'Success' })
    } catch (err) {
        return res.status(err.status).json({ message: err.message })
    }
})

router.post('/join/:id', authentication, async (req, res) => {
    if (!req.body.name || !req.body.theses) return res.status(400).json({ message: 'Invalid args' })
    try {
        const deserializer = new Deserializer()
        const encoded = deserializer.deserialize(req.body.theses)
        const theses = Buffer.from(encoded, 'base64').toString()
        if (!theses.includes('pdf')) return res.status(400).json({ message: 'Invalid theses mimetype' })

        const cp = new ConferenceProxy()
        const conference = await cp.getById(req.params.id)
        const up = new UserProxy()
        const organizer = await up.getById(conference.organizer)
        const organizerEmail = organizer.email
        if (res.locals.user.email === organizerEmail)
            return res.status(400).json({ message: 'You cannot join to yourself' })

        const transactor = new Transactor()
        const sender = new Payer(res.locals.user, transactor)
        const receiver = new Payer(organizer, transactor)
        transactor.register(sender)
        transactor.register(receiver)
        sender.pay(receiver.getUserId())

        const mailAdapter = new MailAdapter({
            from: res.locals.user.email,
            to: organizerEmail,
            subject: 'Theses',
            text: req.body.name,
            attachments: [
                {
                    filename: 'theses.pdf',
                    path: theses
                }
            ]
        })
        await mailAdapter.sendMail()
        return res.json({ message: 'Success' })
    } catch (err) {
        return res.status(err.status).json({ message: err.message })
    }
})

export default router
