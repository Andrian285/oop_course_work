export enum Config {
    JwtSecret = 'JwTSejsxCrREt_2855ls',
    ServerSalt = '45%sAlT_',
    EntitiesPerPage = '4',
    Budget = 83200,
    ParticipationFee = 416,
    Currency = 'uah'
}
